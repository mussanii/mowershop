@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Contact Mowershop</div>

                <div class="card-body">



@if(count($errors) > 0)

<div class="alert alert-danger">
Please correct the following errors:<br>
<ul>
@foreach($errors->all() as $error)

<li>{{$error}}</li>

@endforeach
</ul>
</div>
@endif
<div class="well">

{!! Form::open(array('route'=>'contact_store', 'class'=>'form','novalidate'=>'novalidate')) !!}

<div class="form-group">
  {!! Form::label('Your Name:') !!}

  {!! Form::text('name', null,
    array('required',
    'class'=>'form-control',
    'placeholder'=>'Your name')) !!}

</div>

<div class="form-group">
  {!! Form::label('Your E-mail Address:') !!}

  {!! Form::text('email', null,
    array('required',
    'class'=>'form-control',
    'placeholder'=>'Your email address')) !!}

</div>

<div class="form-group">
  {!! Form::label('Your Message:') !!}

  {!! Form::textarea('message', null,
    array('required',
    'class'=>'form-control',
    'placeholder'=>'Your message')) !!}

</div>
<div class="form-group">
  {!! Form::submit('Say Hello!',
    array('class'=>'btn btn-primary')) !!}

</div>

{!! Form::close() !!}
</div>
                </div>
              </div>
        </div>
      </div>

@endsection