<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Welcome to Mowershop</title>
        <link rel="stylesheet" href="/css/app.css">
       </head>
    <body class="font-sans bg-gray-900 text-white">
    
    <nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark">
  <a class="navbar-brand" href="/">Mowershop Inc</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/about">About</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Products</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/contact">Contact</a>
      </li>
      
      
    </ul>
    <ul class="navbar-nav navbar-right">
      <li class="mr-4"><a href="/auth/register">Register</a></li>
      <li><a href="/auth/login"> Login</a></li>
    </ul>
    
  </div>
  
</nav> 
<div class="container-fluid">
@yield('content')
  
</div>
    
       
       
    </body>
</html>