<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource('about','AboutController',['only'=>['index']]);
Route::get('contact',['as'=>'contact','uses'=>'ContactController@create']);
Route::post('contact',['as'=>'contact_store','uses'=>'ContactController@store']);
//Route::resource('products', 'ProductsController',array('only'=>'index,show'));
Route::get('/products', 'ProductsController@index')->name('products');
Route::get('/products/{$id}', 'ProductsController@show')->name('products.show');




Auth::routes();

//Route::get('/', 'HomeController@index')->name('home');
Route::get('/discounts', 'DiscountsController@index')->name('discounts');

//Admin Group Controllers
Route::group([
    'prefix'=>'admin',
 'namespace'=>'Admin',
 'middleware'=>'admin'
],function()
{
    Route::resource('products','ProductsController');

});
