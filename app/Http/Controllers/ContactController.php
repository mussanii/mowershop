<?php

namespace App\Http\Controllers;
use App\Http\Requests\ContactFormRequest;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contact.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactFormRequest  $request)
    {
        $data = [
            'name'=>$request->get('name'),
            'email'=>$request->get('email'),
            'user_message'=>$request->get('message')
        ];
        \Mail::send('emails.contact', $data, function($message){

            $message->from(env('MAIL_FROM_ADDRESS'));
            $message->to(env('MAIL_FROM_ADDRESS'),env('MAIL_FROM_NAME'));
            $message->subject('Mowershop.com Inquiry');
        });
       //  return \Redirect::route('contact')->with('message','Thanks for contacting us!');
        return redirect('/contact')->with('message','Thanks for contacting us!');
    }

   
}
